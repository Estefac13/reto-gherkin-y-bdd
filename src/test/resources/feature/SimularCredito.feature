# language: es

    Característica: Simulación crédito de libre inversión en la sucursal virtual personas Bancolombia
        Como cliente de Bancolombia
        Necesito simular un crédito de libre inversión
        Para identificar el plan de pagos

    Escenario: Validar ingreso al simulador de créditos
        Dado que el cliente navegó en la sucursal virtual hasta el portafolio de créditos
        Cuando el cliente selecciona simular crédito
        Entonces se mostrará el formulario para simular el crédito


    Escenario: Simular crédito de libre inversión
        Dado que el cliente navegó hasta el simulador de créditos
        Cuando el cliente suministre la información necesaria para crédito de libre inversión y presione simular
        Entonces se mostrará el resultado de la simulación






