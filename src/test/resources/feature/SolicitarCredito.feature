# language: es

    Característica: Solicitud de crédito de libre inversión en sucursal virtual personas Bancolombia
        Como cliente de Bancolombia
        Necesito ingresar al portafolio de créditos de la sucursal virtual personas
        Para solicitar un crédito de libre inversión

    Antecedentes:
        Dado que el cliente navegó hasta la sección de solicitud de créditos

    Escenario: Solicitar crédito libre inversión fallida
        Cuando el cliente el cliente suministre la información incompleta para crédito de libre inversión y presione solicitar
        Entonces se mostrará advertencia de que falta información para solicitar el crédito

    Escenario: Solicitar crédito de libre inversión existoso
        Cuando el cliente suministre la información necesaria para crédito de libre inversión y presione solicitar
        Entonces se mostrará solicitud de crédito enviada exitosamente