# language: es

    Característica:Inicio de sesión en sucursal virtual personas Bancolombia
      Como cliente de Bancolombia
      Necesito iniciar sesión en la sucursal virtual personas de Bancolombia
      Para validar el portafolio de créditos de libre inversión

  Antecedentes:
    Dado que el cliente navegó hasta el inicio de sesión de la sucursal virtual

  Escenario: Inicio de sesión fallido
    Cuando el cliente suministra credenciales incorrectas y confirma
    Entonces se mostrará mensaje de error de inicio de sesión fallida


  Escenario: Inicio de sesión exitoso
    Cuando el cliente suministra credenciales correctas y confirma
    Entonces se ingresará a la sucursal virtual personas de manera exitosa






