# language: es

    Característica: Abono a crédito de libre inversión en la sucursal virtual personas Bancolombia
        Como cliente de Bancolombia
        Necesito ingresar a la sucursal virtual personas
        Para abonar al crédito de libre inversión

    Antecedentes:
        Dado que el cliente navegó hasta el crédito que tiene activo con el banco

    Escenario: Abono parcial a crédito de libre inversión
        Cuando el cliente selecciona la opción pagar, selecciona pago de cuota y confirma
        Entonces se mostrará el resultado de la transacción

    Escenario: Abono total a crédito de libre inversión
        Cuando el cliente selecciona la opción pagar, selecciona pago total del crédito y confirma
        Entonces se mostrará el resultado de la transacción